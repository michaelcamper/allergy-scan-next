import { https } from 'firebase-functions';
import { Product } from './product';
import { CONFIG } from './config';
import { IApiResponse, ICapture } from './api.model';
const cors: any = require('cors')({ origin: true });
const fetch = require('node-fetch');
import { RequestInit } from 'node-fetch';

export const fetchProduct = https.onRequest((req, res) =>
  cors(req, res, async () => {
    const gtin = req.query.gtin;
    if (!gtin) {
      throw new Error('No GTIN provided');
    }
    const endpoint = `/products/${gtin}/?format=json`;
    const { success, products } = await APIRequest<IApiResponse>(endpoint).catch(() => ({
      success: false,
      products: null,
    }));
    if (!success) {
      return res.json(null);
    }
    const [productData] = products;
    const product = new Product(productData);
    return res.json(product);
  })
);

export const reportProduct = https.onRequest((req, res) =>
  cors(req, res, async () => {
    const endpoint = '/product/report/';
    const { gtin, description } = req.body;
    const body = JSON.stringify({
      gtin,
      error_description: description,
      app: CONFIG.app,
    });
    const { success } = await APIRequest<{ success: boolean }>(endpoint, body);
    return res.json(success);
  })
);

export const productCapture = https.onRequest((req, res) =>
  cors(req, res, async () => {
    const { gtin, product } = req.body as { gtin: string; product: ICapture };
    const endpoint = `/crowdsource/product/${gtin}`;
    const response = await APIRequest(endpoint, product).catch(() => {
      return APIRequest(endpoint, product, 'put');
    });
    return res.json(response);
  })
);

async function APIRequest<T>(
  endpoint: string,
  body?: any,
  method: 'get' | 'post' | 'put' = body ? 'post' : 'get'
): Promise<T> {
  const url = CONFIG.baseUrl + endpoint;
  const auth =
    'Basic ' + Buffer.from(CONFIG.username + ':' + CONFIG.password).toString('base64');
  const options: RequestInit = {
    method,
    headers: {
      Authorization: auth,
      'Content-Type': 'application/json',
    },
    body,
  };
  return fetch(url, options)
    .then((res) => res.text())
    .then((text) => {
      console.log(text);
      return JSON.parse(text);
    });
}
