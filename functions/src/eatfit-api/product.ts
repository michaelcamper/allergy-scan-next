import { CONFIG } from './config';
import { Allergen, IProduct } from './api.model';

export class Product {
  gtin: string;
  name: string;
  imgSrc: string;
  allergens: string[];
  constructor(data: IProduct) {
    const langOrder: string[] = ['de', 'en', 'fr', 'it'];
    this.gtin = data.gtin.toString();
    this.name = langOrder.reduce((productName, lang) => {
      return productName || data[`product_name_${lang}`];
    }, null);
    this.imgSrc = data.image;
    this.allergens = Product.prettyAllergens(data.allergens);
  }
  private static prettyAllergens(allergens: Allergen[]): string[] {
    const ALLERGEN_KEYS = [
      'celery',
      'crustacean',
      'eggs',
      'fish',
      'gluten',
      'lupin',
      'milk',
      'molluscs',
      'mustard',
      'peanuts',
      'sesameSeeds',
      'soy',
      'sulphites',
      'treeNuts',
    ];

    function sanitizeName(name: string) {
      const prefix = 'allergen';
      let key = name.replace(prefix, '');
      key = key.charAt(0).toLowerCase() + key.substr(1);
      const MAPPINGS = {
        cellery: 'celery',
        hazel: 'treeNuts',
        wheat: 'gluten',
      };
      return MAPPINGS[key] || key;
    }

    function isDuplicate(array: string[], item: string) {
      return array.indexOf(item) !== -1;
    }
    function isUnknown(array: string[], item: string) {
      return array.indexOf(item) === -1;
    }

    return allergens.reduce<string[]>((keys, { name }) => {
      let key = sanitizeName(name);
      if (isDuplicate(keys, key) || isUnknown(ALLERGEN_KEYS, key)) {
        return keys;
      }
      return [...keys, key];
    }, []);
  }
}
