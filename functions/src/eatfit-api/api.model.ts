export interface IApiResponse {
  products: IProduct[];
  success: boolean;
}

export interface IProduct {
  gtin: number;
  product_name_en: string;
  product_name_de: string;
  product_name_fr: string;
  product_name_it: string;
  serving_size: string;
  comment: string;
  image: string;
  allergens: Allergen[];
}

export interface Allergen {
  name: string;
}

export interface IReport {
  gtin: string;
  error_description: string;
  app: string;
}

export interface ICapture {
  name: string;
  front_image: string;
  back_image: string;
}
