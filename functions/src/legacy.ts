import { firestore } from 'firebase-functions';
import * as admin from 'firebase-admin';
import { User } from '../../models/user';

admin.initializeApp();

/* User Details */
export const dataCollectedLegacy = firestore
  .document('userData/{userID}')
  .onCreate((snapshot) => {
    const details = dataToDetails(snapshot.data().data);
    const user = extractUser(snapshot.ref);
    return user.update({
      details,
      data: snapshot.ref,
    });
  });

function dataToDetails({ physics, behaviour, dieting }): User.IDetails {
  const { gender, year } = physics;
  const { german, travelling } = behaviour;
  return {
    gender,
    year,
    german,
    travelling,
    dietResearch: dieting.research,
  };
}

/* Feedback */
export const feedBackReceivedLegacy = firestore
  .document('userFeedback/{userID}')
  .onCreate(({ ref }) => {
    const user = extractUser(ref);
    return user.update({ feedback: ref });
  });

/* Utils */

function extractUser(ref: any): any {
  return admin
    .firestore()
    .collection('users')
    .doc(ref.id);
}
