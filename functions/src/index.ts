import { productCapture, fetchProduct, reportProduct } from './eatfit-api';
import { feedbackReceived } from './feedback';
import { dataCollectedLegacy, feedBackReceivedLegacy } from './legacy';

/* Eat-Fit API */
exports.productData = fetchProduct;
exports.productReport = reportProduct;
exports.productCapture = productCapture;

/* Feedback */
exports.feedbackReceived = feedbackReceived;

/* Legacy */
exports.legacyData = dataCollectedLegacy;
exports.legacyFeedback = feedBackReceivedLegacy;
