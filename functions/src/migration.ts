import { https } from 'firebase-functions';
import * as admin from 'firebase-admin';
import { User } from '../../models/user';
const cors: any = require('cors')({ origin: true });

admin.initializeApp();

export const dataMigration = https.onRequest((req, res) =>
  cors(req, res, async () => {
    // get all user with data
    const { docs } = await admin
      .firestore()
      .collection('users')
      .orderBy('data')
      .get();
    const batch = admin.firestore().batch();

    for (let doc of docs) {
      const { platform, details, data } = doc.data();

      const update: Partial<User.Document> = {};

      if (platform) {
        update.deviceOS = platform;
        update['platform'] = admin.firestore.FieldValue.delete();
      }
      if (!details) {
        const { physics, behaviour, dieting } = (await data.get()).data().data;
        const { gender, year } = physics;
        const { german, travelling } = behaviour;
        update.details = {
          gender,
          year,
          german,
          travelling,
          dietResearch: dieting.research,
        };
      }
      batch.update(doc.ref, update);
    }
    const result = await batch.commit().catch((err) => err);

    return res.json(result);
  })
);
