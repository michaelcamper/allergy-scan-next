import { firestore } from 'firebase-functions';

export const feedbackReceived = firestore
  .document('feedback/{feedbackID}')
  .onCreate((snapshot) => {
    const user: any = snapshot.data().user;
    return user.update({ feedback: snapshot.ref });
  });
