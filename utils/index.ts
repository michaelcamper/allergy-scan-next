export function enumToArray(enumerator: any): string[] {
  const keys = Object.keys(enumerator);
  const values = keys.filter(
    (key: string | number) => !isNaN(parseInt(key.toString(), 10))
  );
  return values.reduce<string[]>((acc: string[], prev: string) => {
    acc.push(enumerator[prev]);
    return acc;
  }, []);
}
