import { enumToArray } from '../utils';

enum Allergen {
  'celery',
  'crustacean',
  'eggs',
  'fish',
  'gluten',
  'lupin',
  'milk',
  'molluscs',
  'mustard',
  'peanuts',
  'sesameSeeds',
  'soy',
  'sulphites',
  'treeNuts',
}

export const ALLERGENS = enumToArray(Allergen);
