// First entry will be used as default fallback
export const AVAILABLE_LANGUAGES: { key: string; locale: Object }[] = [
  {
    key: 'de',
    locale: require('date-fns/locale/de'),
  },
  {
    key: 'en',
    locale: require('date-fns/locale/en'),
  },
];
