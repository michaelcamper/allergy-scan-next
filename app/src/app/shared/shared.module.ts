import { NgModule } from '@angular/core';
import { LogoComponent } from './logo/logo.component';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { AllergenComponent } from './allergen/allergen.component';
import { BarComponent } from './bar/bar.component';

const COMPONENTS = [LogoComponent, AllergenComponent, BarComponent];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule],
  declarations: COMPONENTS,
  exports: [CommonModule, IonicModule, TranslateModule, ...COMPONENTS],
})
export class SharedModule {}
