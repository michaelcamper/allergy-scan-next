import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import * as i18n from '../../../assets/i18n/de.json';

const deStrings: { [key: string]: string } = i18n['allergens'];

@Component({
  selector: 'app-allergen',
  templateUrl: 'allergen.component.html',
  styleUrls: ['./allergen.component.scss'],
})
export class AllergenComponent {
  public name: string;
  public img: string;
  public view: 'button' | 'text' | 'icon';

  @Input('view')
  set _view(view: 'button' | 'text' | 'icon') {
    this.view = view;
    this[view] = true;
  }

  @HostBinding('class.button')
  button: boolean;
  @HostBinding('class.icon')
  icon: boolean;
  @HostBinding('class.text')
  text: boolean;

  @Input()
  set key(key: string) {
    switch (this.view) {
      case 'text':
        this.name = deStrings[key] || key.toUpperCase();
        break;
      case 'button':
        this.name = `allergens.${key}`;
        break;
    }
    this.img = `assets/imgs/allergens/${key}.png`;
  }

  @Input()
  active: boolean;

  @Output()
  toggle: EventEmitter<void> = new EventEmitter<void>();
}
