import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss'],
})
export class BarComponent {
  @Input() title?: string;
  @Input() color?: string;
  @Input() close?: boolean;

  constructor(public modal: ModalController) {}
}
