import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HistoryComponent } from './history.component';
import { HistoryService } from './history.service';

@NgModule({
  imports: [SharedModule],
  declarations: [HistoryComponent],
  exports: [HistoryComponent],
  entryComponents: [HistoryComponent],
  providers: [HistoryService],
})
export class HistoryModule {}
