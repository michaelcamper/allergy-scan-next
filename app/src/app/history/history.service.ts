import { Injectable } from '@angular/core';
import { Scan } from '../../../../models/scan';
import { ModalController } from '@ionic/angular';
import { HistoryComponent } from './history.component';

@Injectable()
export class HistoryService {
  constructor(private modal: ModalController) {}

  async open(scans: Scan.Entry[]) {
    // TODO consolidateScans;
    const modal = await this.modal.create({
      component: HistoryComponent,
      componentProps: { scans: this.consolidateScans(scans) },
    });
    modal.present();
  }

  private consolidateScans(scans: Scan.Document[]) {
    const hasName = (scan: Scan.Entry) => Boolean(scan.product.name);
    return scans.filter(hasName).reduce<Scan.Entry[]>((entries, scan) => {
      const existingScan = entries.find(({ gtin }) => scan.gtin === gtin);
      if (existingScan) {
        existingScan.count++;
      } else {
        entries.push({
          ...scan,
          count: 1,
        });
      }
      return entries;
    }, []);
  }
}
