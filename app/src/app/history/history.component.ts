import { Component } from '@angular/core';
import { Scan } from '../../../../models/scan';
import { ResultService } from '../result/result.service';
import { distanceInWordsToNow } from 'date-fns';
import { I18NService } from '../core/i18n.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent {
  public scans: Scan.Entry[];

  // @Input('scans')
  // set filterScans(scans: Scan.Result[]) {
  //   if (!scans || scans.length === 0) {
  //     return;
  //   }
  //   const hasName = (run: Scan.Entry) => Boolean(run.product.name);
  //   this.scans = scans.filterFn(hasName).reduce<Scan.Entry[]>((entries, run) => {
  //     const existingScan = entries.find(({ gtin }) => run.gtin === gtin);
  //     console.log(existingScan);
  //     if (existingScan) {
  //       existingScan.count++;
  //     } else {
  //       entries.push({
  //         ...run,
  //         count: 1,
  //       });
  //     }
  //     return entries;
  //   }, []);
  // }

  constructor(
    private result: ResultService,
    private i18n: I18NService,
    private view: ModalController
  ) {}

  open({ gtin, product }: Scan.Entry): void {
    this.result.show(product);
  }

  timeSince(timeStamp: number): string {
    const { locale } = this.i18n;
    return distanceInWordsToNow(timeStamp, {
      addSuffix: true,
      locale,
    });
  }

  close() {
    this.view.dismiss();
  }
}
