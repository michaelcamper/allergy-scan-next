import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { FeedbackModule } from './feedback/feedback.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { ResultModule } from './result/result.module';
import { CaptureModule } from './capture/capture.module';
import { HistoryModule } from './history/history.module';
import { ScansModule } from './scans/scans.module';
import { SharedModule } from './shared/shared.module';
import { ScannerModule } from './scanner/scanner.module';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'md',
    }),
    AppRoutingModule,
    CoreModule,
    ConfigurationModule,
    HistoryModule,
    ResultModule,
    CaptureModule,
    FeedbackModule,
    ScansModule,
    SharedModule,
    ScannerModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
