import { NgModule } from '@angular/core';
import { CaptureService } from './capture.service';
import { CaptureComponent } from './capture.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CameraService } from './camera.service';
import { Camera } from '@ionic-native/camera/ngx';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule],
  declarations: [CaptureComponent],
  entryComponents: [CaptureComponent],
  providers: [CaptureService, CameraService, Camera],
})
export class CaptureModule {}
