import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CaptureComponent } from './capture.component';
import { APIService } from '../core/api.service';
import { ToastService } from '../toast.service';

@Injectable()
export class CaptureService {
  constructor(
    private modal: ModalController,
    private api: APIService,
    private toast: ToastService
  ) {}

  public async start(gtin: string) {
    const capture = await this.open();
    if (!capture) {
      return;
    }
    const res = await this.api.captureProduct(gtin, capture);
    console.log(res);
    await this.toast.show('THANK_YOU', 'secondary', 2000);
  }

  private async open(): Promise<ICapture> {
    const modal = await this.modal.create({
      component: CaptureComponent,
    });
    await modal.present();
    return (await modal.onWillDismiss()).data as ICapture;
  }
}

export interface ICapture {
  name: string;
  front_image: string;
  back_image: string;
}
