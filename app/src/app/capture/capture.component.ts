import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ICapture } from './capture.service';
import { IonSlides, ModalController } from '@ionic/angular';
import { startWith, switchMap } from 'rxjs/operators';
import { merge, Subscription } from 'rxjs';
import { CameraService } from './camera.service';

@Component({
  selector: 'app-capture',
  templateUrl: './capture.component.html',
  styleUrls: ['./capture.component.scss'],
})
export class CaptureComponent implements AfterViewInit, OnDestroy {
  form: FormGroup;
  @ViewChild(IonSlides)
  slider: IonSlides;
  get nameControl(): AbstractControl {
    return this.form.get('name');
  }
  get invalidName(): boolean {
    return this.nameControl.invalid;
  }
  public busy: boolean;
  private swipeLocker: Subscription;

  constructor(private camera: CameraService, private modal: ModalController) {
    this.form = CaptureComponent.generateForm();
  }
  ngAfterViewInit(): void {
    this.slideGuard();
  }

  private slideGuard(): void {
    const controls: (keyof ICapture)[] = ['name', 'front_image', 'back_image'];
    const interaction = merge(this.form.valueChanges, this.slider.ionSlideDidChange);
    this.swipeLocker = interaction
      .pipe(
        startWith(null),
        switchMap(() =>
          this.slider.getActiveIndex().then((index) => {
            const controlName = controls[index];
            const control = this.form.get(controlName);
            return control.invalid;
          })
        )
      )
      .subscribe(this.slider.lockSwipeToNext);
  }

  ngOnDestroy(): void {
    this.swipeLocker.unsubscribe();
  }

  public async next() {
    const isEnd = await this.slider.isEnd();
    if (isEnd) {
      return this.submit();
    }
    this.slider.slideNext();
  }

  public async takePhoto(name: keyof ICapture) {
    const control = this.form.get(name);
    this.busy = true;
    const blob = await this.camera.take();
    control.setValue(blob);
    this.busy = false;
    this.next();
  }

  private static generateForm(): FormGroup {
    const controls: { [P in keyof ICapture]: FormControl } = {
      name: new FormControl('', Validators.required),
      front_image: new FormControl(null, Validators.required),
      back_image: new FormControl(null, Validators.required),
    };
    return new FormGroup(controls);
  }

  private submit(): void {
    const capture = this.form.value as ICapture;
    this.modal.dismiss(capture);
  }
}
