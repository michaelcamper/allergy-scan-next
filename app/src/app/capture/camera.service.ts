import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { picture } from './mock';

@Injectable()
export class CameraService {
  get options(): CameraOptions {
    return {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      correctOrientation: false,
    };
  }
  constructor(private camera: Camera) {}

  async take(): Promise<Blob> {
    const base64 = await this.camera.getPicture(this.options).catch((err) => {
      switch (err) {
        case 'cordova_not_available':
          return picture;
        default:
          console.error(err);
      }
    });
    return CameraService.base64toBlob(base64);
  }

  private static base64toBlob(base64: string): Blob {
    const byteString: string = atob(base64);
    const mimeString: string = 'image/jpeg';
    const byteArrays: Uint8Array[] = [];
    for (let offset: number = 0; offset < byteString.length; offset += 512) {
      const slice: string = byteString.slice(offset, offset + 512);
      const byteNumbers: any[] = new Array(slice.length);
      for (let i: number = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray: Uint8Array = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: mimeString });
  }
}
