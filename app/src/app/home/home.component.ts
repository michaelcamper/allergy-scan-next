import { Component, ViewChild } from '@angular/core';
import { UserService } from '../core/user.service';
import { I18NService } from '../core/i18n.service';
import { Scan } from '../../../../models/scan';
import { ResultService } from '../result/result.service';
import { ScannerComponent } from '../scanner/scanner/scanner.component';
import { CaptureService } from '../capture/capture.service';
import { Observable } from 'rxjs';
import { FeedbackService } from '../feedback/feedback.service';
import { ScansService } from '../scans/scans.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  ready$: Promise<void[]> = Promise.all([this.i18n.setLang(), this.user.initialize()]);
  @ViewChild(ScannerComponent) scanner: ScannerComponent;
  constructor(
    private user: UserService,
    private i18n: I18NService,
    private result: ResultService,
    private capture: CaptureService,
    private feedback: FeedbackService,
    private scansService: ScansService
  ) {}

  get scans$(): Observable<Scan.Document[]> {
    return this.user.scans$;
  }

  async onScan(scan: Scan.Result) {
    this.user.logScan(scan);
    const { product, gtin } = scan;
    await (product ? this.result.show(product) : this.capture.start(gtin));
    this.scanner.reset();
    return this.feedback.check();
  }

  showScans(scans: Scan.Entry[]) {
    this.scansService.showScans(scans);
  }
}
