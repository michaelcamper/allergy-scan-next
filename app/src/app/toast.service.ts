import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { I18NService, LazyKey } from './core/i18n.service';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private toast: ToastController, private i18n: I18NService) {}
  async show(key: LazyKey, color: 'primary' | 'secondary' | 'danger', duration: number) {
    const message = await this.i18n.getTranslation(key);
    const toast = await this.toast.create({
      message,
      duration,
      position: 'top',
      color,
    });
    await toast.present();
    return toast;
  }
}
