import { Injectable } from '@angular/core';
import { I18NService } from './i18n.service';
import { StorageService } from './storage.service';
import { IAppConfig } from '../../../../models/config';
import { ITestGroup } from '../../../../models/user';
import { LegacyService } from './legacy.service';
import { Observable } from 'rxjs';
import { Scan } from '../../../../models/scan';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';
import { DatabaseService } from './database.service';
import { FeedbackService } from '../feedback/feedback.service';
import { ConfigurationService } from '../configuration/configuration.service';
import { AllergyProfile } from '../../../../models/allergy-profile';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public testGroup: ITestGroup;
  public allergies: AllergyProfile;
  public scans$: Observable<Scan.Document[]>;

  constructor(
    private i18n: I18NService,
    private feedback: FeedbackService,
    private db: DatabaseService,
    private storage: StorageService,
    private configuration: ConfigurationService,
    private legacy: LegacyService
  ) {
    // this.i18n.setLang();
  }

  async initialize(): Promise<void> {
    await this.configure();
    const isValid = await this.validate();
    if (!isValid) {
      await this.storage.reset();
      return this.initialize();
    }
    if (this.legacy.detailsMissing) {
      await this.register();
    }
    this.scans$ = this.db.fetchScans().pipe(
      distinctUntilChanged((a, b) => a.length === b.length),
      tap((scans) => this.feedback.listen(scans)),
      map((scans) =>
        scans.reduce<Scan.Entry[]>((scans, scan: Scan.Document) => {
          if (scan.product && scan.product.name) {
            const existingScan = scans.find(({ gtin }) => gtin === scan.gtin);
            if (existingScan) {
              existingScan.count++;
            } else {
              scans.push({
                ...scan,
                count: 1,
              });
            }
          }
          return scans;
        }, [])
      )
    );
  }

  private async configure(): Promise<void> {
    const { docPath, allergies, testGroup, lang } = await this.storage
      .get()
      .catch(() => this.register());
    this.i18n.setLang(lang);
    this.db.loadUser(docPath);
    this.allergies = allergies;
    this.testGroup = testGroup;
  }

  private async validate(): Promise<boolean> {
    try {
      const user = await this.db.getUser();
      this.legacy.lookForData(user);
      this.feedback.lookForFeedback(user);
      return true;
    } catch (e) {
      return false;
    }
  }

  private async register(): Promise<IAppConfig> {
    const { config, details, deviceOS } = await this.configuration.init();
    const docPath = await this.db.createUser(config, details, deviceOS);
    return this.storage.set(config, docPath);
  }

  logScan(scan: Scan.Result) {
    return this.db.logScan(scan);
  }
}
