import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AVAILABLE_LANGUAGES } from '../../../../config/i18n';
import { environment } from '../../environments/environment';

export type LazyKey = 'CAMERA_ACCESS' | 'THANK_YOU';

@Injectable({
  providedIn: 'root',
})
export class I18NService {
  get lang(): string {
    return this.translate.currentLang;
  }
  get locale(): Object {
    return AVAILABLE_LANGUAGES.find(({ key }) => key === this.lang).locale;
  }
  constructor(private translate: TranslateService) {
    if (!AVAILABLE_LANGUAGES || AVAILABLE_LANGUAGES.length === 0) {
      throw new Error('No languages defined. Define them in "config/i18n.ts"');
    }
    this.translate.addLangs(AVAILABLE_LANGUAGES.map(({ key }) => key));
    this.translate.setDefaultLang(AVAILABLE_LANGUAGES[0].key);
  }

  setLang(lang?: string) {
    // if (!environment.production) {
    //   lang = prompt('Set Language (de|en)');
    // }
    if (lang === undefined) {
      lang = this.detectLanguage();
    }
    this.translate.use(lang);
  }

  getTranslation(key: LazyKey): Promise<any> {
    return this.translate.get('lazy.' + key).toPromise();
  }

  private detectLanguage() {
    const browserLang = this.translate.getBrowserLang().substr(0, 2);
    const languageIsAvailable = this.translate.langs.includes(browserLang);
    return languageIsAvailable ? browserLang : this.translate.defaultLang;
  }
}
