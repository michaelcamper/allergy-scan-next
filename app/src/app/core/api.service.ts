import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../../../../functions/src/eatfit-api/product';
import { ICapture } from '../capture/capture.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIService {
  private baseUrl = 'https://us-central1-allergy-scan.cloudfunctions.net/';
  constructor(private http: HttpClient) {}

  public async fetchProduct(gtin: string): Promise<Product> {
    const endpoint = `productData?gtin=${gtin}`;
    return this.request<Product>(endpoint);
  }

  public async reportProduct(gtin: string, description: string) {
    const endpoint = 'productReport';
    const body: IProductReport = {
      gtin,
      description,
    };
    return this.request<Product, IProductReport>(endpoint, body);
  }

  public captureProduct(gtin: string, product: ICapture) {
    return this.request('productCapture', { gtin, product });
  }

  private request<Response, Request = null>(
    endpoint: string,
    body?: Request
  ): Promise<Response> {
    const url = this.baseUrl + endpoint;
    const method: 'get' | 'post' = body ? 'post' : 'get';
    const request = {
      get: this.http.get(url),
      post: this.http.post(url, body),
    }[method];
    return request.toPromise() as Promise<Response>;
  }
}

export interface IProductReport {
  gtin: string;
  description: string;
}
