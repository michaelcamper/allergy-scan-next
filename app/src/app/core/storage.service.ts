import { Injectable } from '@angular/core';
import { IAppConfig } from '../../../../models/config';
import { Storage } from '@ionic/storage';
import { User } from '../../../../models/user';
import { environment } from '../../environments/environment';

const MOCK_CONFIG: IAppConfig = {
  lang: 'en',
  allergies: {
    gluten: true,
    milk: true,
    peanuts: true,
  },
  testGroup: {
    tailored: true,
    view: 'text',
  },
  docPath: '/usersDev/ZuXMKoSp1CsObexp8Zj1',
};

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private key = 'config';
  constructor(private storage: Storage) {}

  async get(): Promise<IAppConfig> {
    const config: IAppConfig = await this.storage.get(this.key);
    if (!environment.production) {
      return MOCK_CONFIG;
    }
    if (config) {
      return config;
    }
    return Promise.reject();
  }

  async set(userConfig: User.IConfig, docPath: string): Promise<IAppConfig> {
    const config: IAppConfig = {
      ...userConfig,
      docPath,
    };
    await this.storage.set(this.key, config);
    return config;
  }

  async reset(): Promise<void> {
    await this.storage.remove(this.key);
  }
}
