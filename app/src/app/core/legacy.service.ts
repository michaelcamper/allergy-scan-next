import { Injectable } from '@angular/core';
import { User } from '../../../../models/user';

@Injectable({
  providedIn: 'root',
})
export class LegacyService {
  public detailsMissing: boolean;

  public lookForData(user: User.Document) {
    this.detailsMissing = !Boolean(user.data) && !Boolean(user.details);
  }

  static dateToTimeStamp({ timeStamp, date }: { timeStamp?: number; date?: any }) {
    return timeStamp || date.toDate().getTime();
  }
}

/*
TODO Merge User and UserData Docs
 */
