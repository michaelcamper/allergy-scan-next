import { Injectable } from '@angular/core';
import { DeviceOS, User } from '../../../../models/user';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Scan } from '../../../../models/scan';
import { map } from 'rxjs/operators';
import { LegacyService } from './legacy.service';
import { Feedback } from '../../../../models/feedback';
import { environment } from '../../environments/environment';

const COLLECTIONS = {
  users: environment.production ? 'users' : 'usersDev',
  scans: 'scans',
  feedback: environment.production ? 'feedback' : 'feedbackDev',
};

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  private user: AngularFirestoreDocument<User.Document>;
  constructor(private afs: AngularFirestore) {}

  loadUser(path: string): void {
    this.user = this.afs.doc<User.Document>(path);
  }

  async getUser(): Promise<User.Document> {
    const snap = await this.user.ref.get();
    if (!snap.exists) {
      throw 'notExisting';
    }
    return snap.data() as User.Document;
  }

  async createUser(
    config: User.IConfig,
    details: User.IDetails,
    deviceOS: DeviceOS
  ): Promise<string> {
    const timeStamp = new Date().getTime();
    const user = {
      timeStamp,
      deviceOS,
      ...config,
      details,
    };
    const ref = await this.afs.collection<User.Document>(COLLECTIONS.users).add(user);
    return ref.path;
  }

  fetchScans(): Observable<Scan.Document[]> {
    const userPath = this.user.ref.path;
    return this.afs
      .doc(userPath)
      .collection<Scan.Document>(COLLECTIONS.scans)
      .valueChanges()
      .pipe(
        map((scans: Scan.Document[]) => scans.filter(isSuccessful).sort(sortByTimeStamp))
      );

    function isSuccessful({ product }: Scan.Document): boolean {
      return Boolean(product);
    }
    function sortByTimeStamp(a: Scan.Document, b: Scan.Document) {
      return LegacyService.dateToTimeStamp(b) - LegacyService.dateToTimeStamp(a);
    }
  }

  async logScan(scan: Scan.Result) {
    await this.user
      .collection<Scan.Document>(COLLECTIONS.scans)
      .add({ ...scan, timeStamp: Date.now() });
  }

  async storeFeedback(data: Feedback.Data) {
    const user = this.user.ref;
    const feedback = await this.afs
      .collection<Feedback.Document>(COLLECTIONS.feedback)
      .add({
        timeStamp: Date.now(),
        user,
        data,
      });
    await this.user.update({ feedback });
  }

  async updateProductImage(gtin: string, imgSrc: string) {
    // get all scans with this gtin:
    const { docs } = await this.user
      .collection(COLLECTIONS.scans)
      .ref.where('gtin', '==', gtin)
      .get();
    docs.forEach(({ ref }) => {
      ref.update('product.imgSrc', imgSrc);
    });
  }
}
