import { Injectable } from '@angular/core';
import { LegacyService } from '../core/legacy.service';
import * as format from 'date-fns/format';
import { Scan } from '../../../../models/scan';
import { User } from '../../../../models/user';
import { FeedbackComponent } from './feedback.component';
import { DatabaseService } from '../core/database.service';
import { ModalController } from '@ionic/angular';

@Injectable()
export class FeedbackService {
  private state: 'due' | 'given';
  private checked: boolean;
  private required = {
    scansCount: 3,
    daysCount: 2,
  };

  constructor(private modal: ModalController, private db: DatabaseService) {}

  lookForFeedback(user: User.Document): void {
    const gaveFeedback = Boolean(user.feedback);
    if (gaveFeedback) {
      this.state = 'given';
    }
  }

  async check() {
    this.checked = true;
    if (this.state === 'due') {
      return this.prompt();
    }
  }

  listen(scans: Scan.Document[]) {
    if (this.state === 'given') {
      return;
    }
    if (this.isDue(scans)) {
      this.state = 'due';
    }
    if (!this.checked) {
      this.check();
    }
  }

  private isDue(scans: Scan.Document[]): boolean {
    const uniqueScans = scans.reduce((uniqueGtins, { gtin }) => {
      if (!uniqueGtins.includes(gtin)) {
        uniqueGtins.push(gtin);
      }
      return uniqueGtins;
    }, []);
    const scansCount = uniqueScans.length;
    if (scansCount < this.required.scansCount) {
      return false;
    }
    const daysCount: number = scans.reduce<string[]>((days, scan) => {
      const timeStamp = LegacyService.dateToTimeStamp(scan);
      const day = format(timeStamp, 'YY-MM-DD');
      if (!days.includes(day)) {
        days.push(day);
      }
      return days;
    }, []).length;
    return daysCount >= this.required.daysCount;
  }

  private async prompt() {
    const modal = await this.modal.create({
      component: FeedbackComponent,
    });
    await modal.present();
    const result = await modal.onDidDismiss();
    const { data } = result;
    if (!data) {
      return this.prompt();
    }
    await this.db.storeFeedback(data);
    this.state = 'given'; // TODO automate;
  }
}
