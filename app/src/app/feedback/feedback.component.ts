import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonSlides, ModalController } from '@ionic/angular';
import { filter, map, startWith, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Feedback } from '../../../../models/feedback';

const VALUES = new Array(5)
  .fill(null)
  .map((value, index) => index + 1)
  .reverse();

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackComponent {
  form: FormGroup;
  sections: { sectionName: string; control: FormGroup }[];
  values: number[] = VALUES;

  @ViewChild(IonSlides) slides: IonSlides;

  private get _sections(): {
    sectionName: string;
    control: FormGroup;
  }[] {
    const { controls } = this.form;
    return Object.keys(controls)
      .filter((sectionName) => sectionName !== 'comment')
      .map((sectionName) => ({
        sectionName,
        control: controls[sectionName] as FormGroup,
      }));
  }

  constructor(private modal: ModalController) {
    this.form = FeedbackComponent.generateForm();
    this.sections = this._sections;
  }

  get progress$(): Observable<number> {
    return this.form.valueChanges.pipe(
      startWith(this.form.value),
      map((value: Feedback.Data) => {
        const constructKeys = Object.keys(value);
        const state = constructKeys.reduce(
          (state, constructKey) => {
            const construct = value[constructKey];
            for (let controlKey in construct) {
              state.total++;
              if (construct[controlKey] !== null) {
                state.valid++;
              }
            }
            return state;
          },
          {
            valid: 0,
            total: 0,
          }
        );
        return state.valid / state.total;
      })
    );

    // const sectionKeys = Object.keys(this.form.controls);
    // sectionKeys.reduce(
    //   (state, sectionKey) => {
    //     const section = this.form.get(sectionKey) as FormGroup;
    //     for (let controlKey in section.controls) {
    //       const control = section.controls[controlKey];
    //       control.valid;
    //       console.log(sectionKey, control);
    //     }
    //     return state;
    //   },

    // );
    // console.log(this.form.controls);
  }

  ionViewDidEnter() {
    this.sections.forEach(({ control }) => {
      control.statusChanges
        .pipe(
          filter((status) => status === 'VALID'),
          take(1)
        )
        .subscribe(() => this.slides.slideNext());
    });
  }

  public controls(group: FormGroup): string[] {
    const { controls } = group;
    return Object.keys(controls);
  }

  public i18nString(sectionName: string, controlName: string, value?: number): string {
    return [
      'feedback',
      sectionName,
      controlName,
      value ? value.toString() : 'label',
    ].join('.');
  }

  private static generateForm(): FormGroup {
    const effortExpectancy = new FormGroup({
      usage: new FormControl(null, Validators.required),
      learn: new FormControl(null, Validators.required),
      format: new FormControl(null, Validators.required),
    });
    const performanceExpectation = new FormGroup({
      support: new FormControl(null, Validators.required),
      time: new FormControl(null, Validators.required),
      trust: new FormControl(null, Validators.required),
    });
    const intentionToUse = new FormGroup({
      rating: new FormControl(null, Validators.required),
      regular: new FormControl(null, Validators.required),
      travelling: new FormControl(null, Validators.required),
      recommend: new FormControl(null, Validators.required),
    });
    const comment = new FormControl('');
    return new FormGroup({
      intentionToUse,
      effortExpectancy,
      performanceExpectation,
      comment,
    });
  }

  public submit() {
    const data = this.form.value;
    this.modal.dismiss(data);
  }
}
