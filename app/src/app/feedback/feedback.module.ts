import { NgModule } from '@angular/core';
import { FeedbackService } from './feedback.service';
import { FeedbackComponent } from './feedback.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule],
  declarations: [FeedbackComponent],
  entryComponents: [FeedbackComponent],
  providers: [FeedbackService],
})
export class FeedbackModule {}
