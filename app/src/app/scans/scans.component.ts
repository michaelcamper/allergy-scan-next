import { Component } from '@angular/core';
import { Scan } from '../../../../models/scan';
import { Product } from '../../../../functions/src/eatfit-api/product';
import { ResultService } from '../result/result.service';
import { APIService } from '../core/api.service';
import { DatabaseService } from '../core/database.service';

@Component({
  selector: 'app-scans',
  templateUrl: './scans.component.html',
  styleUrls: ['./scans.component.scss'],
})
export class ScansComponent {
  constructor(
    private result: ResultService,
    private api: APIService,
    private db: DatabaseService
  ) {}

  scans: Scan.Entry[];

  showProduct(product: Product) {
    this.result.show(product);
  }

  async imageNotFound({ product, gtin }: Scan.Entry) {
    console.log('not found');
    const { imgSrc } = await this.api.fetchProduct(gtin);
    if (product.imgSrc === imgSrc) {
      return;
    }
    product.imgSrc = imgSrc;
    this.db.updateProductImage(gtin, imgSrc);
  }
}
