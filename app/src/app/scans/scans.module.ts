import { NgModule } from '@angular/core';
import { ScansComponent } from './scans.component';
import { SharedModule } from '../shared/shared.module';
import { ScansService } from './scans.service';

@NgModule({
  imports: [SharedModule],
  declarations: [ScansComponent],
  entryComponents: [ScansComponent],
  providers: [ScansService],
})
export class ScansModule {}
