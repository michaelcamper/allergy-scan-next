import { Injectable } from '@angular/core';
import { ScansComponent } from './scans.component';
import { ModalController } from '@ionic/angular';
import { Scan } from '../../../../models/scan';

@Injectable()
export class ScansService {
  constructor(private modal: ModalController) {}

  async showScans(scans: Scan.Entry[]) {
    const modal = await this.modal.create({
      component: ScansComponent,
      componentProps: { scans: scans },
    });
    await modal.present();
  }
}
