import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import {
  BarcodeScanner,
  BarcodeScannerOptions,
  BarcodeScanResult,
} from '@ionic-native/barcode-scanner/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AlertController } from '@ionic/angular';
import { I18NService } from '../../core/i18n.service';
import { APIService } from '../../core/api.service';
import { Scan } from '../../../../../models/scan';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss'],
})
export class ScannerComponent {
  busy: boolean;
  @Output() scanned: EventEmitter<Scan.Result> = new EventEmitter<Scan.Result>();

  @ViewChild(LoaderComponent) loader: LoaderComponent;

  constructor(
    private bs: BarcodeScanner,
    private diagnostic: Diagnostic,
    private alert: AlertController,
    private i18n: I18NService,
    private api: APIService
  ) {}

  async run() {
    this.busy = true;
    let cameraAccess;
    try {
      cameraAccess = await this.accessCamera();
    } catch (err) {
      switch (err) {
        case 'cordova_not_available':
          console.log('Camera Access was not checked, because Cordova is not available');
          break;
        default:
          ScannerComponent.logError(err);
      }
      cameraAccess = true;
    }
    if (!cameraAccess) {
      return;
    }
    const gtin = await this.open();
    if (!gtin) {
      return this.reset();
    }
    const product = await this.api.fetchProduct(gtin);
    const success = Boolean(product);
    await this.loader.stop(success);
    this.scanned.emit({ gtin, product });
  }

  reset() {
    this.busy = false;
  }

  private async accessCamera() {
    const hasAccess = await this.diagnostic.isCameraAuthorized();
    if (hasAccess) {
      return true;
    }
    const permissionStatus: string = await this.diagnostic.getCameraAuthorizationStatus();
    switch (permissionStatus) {
      case this.diagnostic.permissionStatus.NOT_REQUESTED:
        await this.cameraAlert('initial');
        await this.diagnostic
          .requestCameraAuthorization()
          .catch(ScannerComponent.logError);
        return this.accessCamera();
      case this.diagnostic.permissionStatus.DENIED:
      case this.diagnostic.permissionStatus.DENIED_ALWAYS:
        await this.cameraAlert('denied');
        break;
      default:
        ScannerComponent.logError(JSON.stringify(permissionStatus));
    }
    return false;
  }

  private async open(): Promise<string | any> {
    const SCANNER_OPTIONS: BarcodeScannerOptions = {
      orientation: 'portrait',
      resultDisplayDuration: 0,
      disableAnimations: false,
      showFlipCameraButton: false,
      showTorchButton: true,
      disableSuccessBeep: true,
    };
    const { cancelled, text }: BarcodeScanResult = await this.bs
      .scan(SCANNER_OPTIONS)
      .catch(ScannerComponent.scannerFallback);

    if (text && !cancelled) {
      return text;
    }
  }

  private async cameraAlert(type: 'initial' | 'denied'): Promise<void> {
    const { header, message, confirm, settings, cancel } = await this.i18n.getTranslation(
      'CAMERA_ACCESS'
    );
    const buttons = {
      initial: [confirm],
      denied: [
        cancel,
        {
          text: settings,
          handler: () => this.diagnostic.switchToSettings(),
        },
      ],
    }[type];
    const alertInstance = await this.alert.create({
      header: header,
      message: message,
      buttons,
      backdropDismiss: false,
    });
    await alertInstance.present();
    await alertInstance.onDidDismiss();
  }

  // Dev
  private static logError(err: any) {
    let message = err;
    if (err.code) {
      message += ` [${err.code}]`;
    }
    alert(message);
    console.log(message);
  }
  private static async scannerFallback(err: any): Promise<BarcodeScanResult> {
    switch (err) {
      case 'cordova_not_available':
        const gtin = prompt('Barcode', '7610200017819');
        return {
          text: gtin,
          format: null,
          cancelled: false,
        };
      default:
        ScannerComponent.logError(err);
        return {
          text: null,
          format: null,
          cancelled: true,
        };
    }
  }
}
