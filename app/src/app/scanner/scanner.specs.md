# Scanner Feature Module

## Purpose
Scan Product via UPC

## Dependancies
- BarcodeScanner: Scanner functionality
- Diagnostics: Check Camera Access
- Network: Check if Network is connected

## Components
- **app-scanner**
- app-loader


##Outputs
- Product scanned
