import { Component } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent {
  bars = ['l', 's', 's', 's', 'l', 'l', 's', 's', 's', 'l'];

  private success: boolean;

  get state(): 'success' | 'failed' | 'busy' {
    switch (this.success) {
      case true:
        return 'success';
      case false:
        return 'failed';
      default:
        return 'busy';
    }
  }

  constructor() {}

  stop(success: boolean) {
    this.success = success;
    // return timer(500).toPromise();
  }
}
