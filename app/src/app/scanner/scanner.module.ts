import { NgModule } from '@angular/core';
import { ScannerComponent } from './scanner/scanner.component';
import { LoaderComponent } from './loader/loader.component';
import { SharedModule } from '../shared/shared.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@NgModule({
  imports: [SharedModule],
  declarations: [ScannerComponent, LoaderComponent],
  exports: [ScannerComponent],
  providers: [BarcodeScanner, Diagnostic],
})
export class ScannerModule {}
