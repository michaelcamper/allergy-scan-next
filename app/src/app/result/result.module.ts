import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ResultComponent } from './result.component';
import { ResultService } from './result.service';
import { ReportService } from './report.service';

@NgModule({
  imports: [SharedModule],
  declarations: [ResultComponent],
  entryComponents: [ResultComponent],
  providers: [ResultService, ReportService],
})
export class ResultModule {}
