import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { APIService } from '../core/api.service';
import { ToastService } from '../toast.service';

@Injectable()
export class ReportService {
  constructor(
    private api: APIService,
    private alert: AlertController,
    private toast: ToastService
  ) {}

  public async report(gtin: string) {
    const description = await this.open();
    if (!description) {
      return;
    }
    const x = await this.api.reportProduct(gtin, description);
    await this.toast.show('THANK_YOU', 'secondary', 2000);
  }

  private async open(): Promise<string> {
    const inputName = 'description';
    const options = {
      inputs: [
        {
          name: inputName,
          type: 'text' as 'text',
          placeholder: "What's wrong?",
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Report',
        },
      ],
    };
    const alert = await this.alert.create(options);
    await alert.present();
    const { data, role } = await alert.onWillDismiss();

    if (role === 'cancel') {
      return;
    }
    return data.values[inputName];
  }
}
