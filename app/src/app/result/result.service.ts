import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Product } from '../../../../functions/src/eatfit-api/product';
import { ResultComponent } from './result.component';
import { IResult } from '../../../../models/result';
import { UserService } from '../core/user.service';
import { APIService } from '../core/api.service';

@Injectable()
export class ResultService {
  constructor(
    private modal: ModalController,
    private user: UserService,
    private api: APIService
  ) {}

  public async show(data: Product): Promise<any> {
    if (data.imgSrc) {
      await this.preloadImage(data.imgSrc).catch(() => {
        data.imgSrc = null;
        this.api.reportProduct(data.gtin, 'Invalid Image Source');
      });
    }
    const { view, tailored } = this.user.testGroup;

    if (tailored) {
      data.allergens = this.filteredAllergens(data.allergens);
    }
    const modal = await this.modal.create({
      component: ResultComponent,
      componentProps: <IResult>{
        ...data,
        view,
        tailored,
      },
    });
    await modal.present();
    return modal.onDidDismiss();
  }

  private async preloadImage(src: string): Promise<void> {
    const image = new Image();
    return new Promise<void>((resolve, reject) => {
      image.src = src;
      image.onerror = () => reject();
      image.onload = () => resolve();
    });
  }

  private filteredAllergens(allergens: string[]): string[] {
    const allergyProfile = this.user.allergies;
    function hasAllergy(allergen: string): boolean {
      return allergyProfile[allergen];
    }
    return allergens.filter(hasAllergy);
  }
}
