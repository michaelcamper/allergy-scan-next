import { ChangeDetectionStrategy, Component } from '@angular/core';
import { IResult } from '../../../../models/result';
import { ViewMode } from '../../../../models/user';
import { ModalController } from '@ionic/angular';
import { ReportService } from './report.service';
import { UserService } from '../core/user.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultComponent implements IResult {
  allergens: string[];
  gtin: string;
  imgSrc: string;
  name: string;
  view: ViewMode;
  tailored: boolean;

  // public get title(): string {
  //   const hasNoImage = !Boolean(this.imgSrc);
  //   if (!hasNoImage) {
  //     return this.name;
  //   }
  // }
  public get productImage(): string {
    return this.imgSrc || 'assets/imgs/product-placeholder.png';
  }
  public get hasNoImage(): boolean {
    return !Boolean(this.imgSrc);
  }
  public get allergensFound(): boolean {
    return this.allergens.length > 0;
  }

  public get alertBoxColor(): 'success' {
    if (this.view === 'icon') {
      return 'success';
    }
  }

  get disclaimer(): string {
    return `disclaimer.${this.tailored ? 'tailored' : 'default'}`;
  }

  constructor(
    private user: UserService,
    private modal: ModalController,
    private errorReport: ReportService
  ) {}

  dismiss(): void {
    this.modal.dismiss();
  }

  public async reportError() {
    await this.errorReport.report(this.gtin);
  }
}
