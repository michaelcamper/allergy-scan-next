import { NgModule } from '@angular/core';
import { ConfigurationComponent } from './configuration.component';
import { SharedModule } from '../shared/shared.module';
import { ConfigurationService } from './configuration.service';
import { AllergyProfileComponent } from './allergy-profile/allergy-profile.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DigitKeyboardComponent } from './digit-keyboard/digit-keyboard.component';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule],
  declarations: [
    ConfigurationComponent,
    AllergyProfileComponent,
    UserDetailsComponent,
    DigitKeyboardComponent,
  ],
  entryComponents: [
    ConfigurationComponent,
    DigitKeyboardComponent,
  ],
  providers: [ConfigurationService],
})
export class ConfigurationModule {}
