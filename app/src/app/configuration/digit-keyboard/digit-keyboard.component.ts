import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  Output,
} from '@angular/core';
import { KeyboardOptions } from './digit-keyboard.model';

@Component({
  selector: 'app-digit-keyboard',
  templateUrl: 'digit-keyboard.component.html',
  styleUrls: ['./digit-keyboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DigitKeyboardComponent {
  @HostBinding('class.open')
  open: boolean;
  @Output()
  set: EventEmitter<any> = new EventEmitter<any>();

  public label?: string;
  public value: number;
  public max?: number;
  public min?: number;
  public unit?: string;
  private length?: number;
  public digits: number[] = new Array(9)
    .fill(null)
    .map((val, index) => index + 1);

  private set options({
    label,
    value,
    unit,
    range,
  }: KeyboardOptions) {
    this.label = label;
    this.value = value;
    this.unit = unit;
    if (range) {
      this.min = range[0];
      this.max = range[1];
      const minLength: number = this.min
        ? this.min.toString().length
        : undefined;
      const maxLength: number = this.max
        ? this.max.toString().length
        : undefined;
      if (
        minLength &&
        maxLength &&
        minLength === maxLength
      ) {
        this.length = minLength;
      }
    }
  }

  public prompt(options: KeyboardOptions): void {
    this.options = options;
    this.open = true;
    this.cdr.detectChanges();
  }

  public get displayValue(): string | number {
    if (this.length) {
      const missingDigits: number =
        this.length - this.stringValue.length;
      if (missingDigits) {
        return (
          this.stringValue +
          new Array(missingDigits).fill(' •').join(' ')
        );
      }
    }
    return this.value;
  }
  public get stringValue(): string {
    return this.value ? this.value.toString() : '';
  }

  public get valid(): boolean {
    if (
      Number.isInteger(this.min) &&
      this.value < this.min
    ) {
      return false;
    }
    if (
      Number.isInteger(this.max) &&
      this.value > this.max
    ) {
      return false;
    }
    return Boolean(this.value);
  }

  constructor(private cdr: ChangeDetectorRef) {}

  public addDigit(digit: number): void {
    const currentValue: string = this.value
      ? this.value.toString()
      : '';
    const value: string = currentValue + digit;
    this.value = parseInt(value);
    if (
      this.length &&
      this.length === this.stringValue.length
    ) {
      this.submit();
    }
  }
  public removeDigit() {
    const value: string = this.value
      .toString()
      .slice(0, -1);
    this.value = parseInt(value) || undefined;
  }
  public async submit(): Promise<void> {
    this.open = false;
    this.set.emit(this.value);
    // await this.modal.dismiss(this.value);
    // this.reset();
  }

  public disabled(digit?: number): boolean {
    if (!this.value && !digit) {
      return true;
    }
    if (!Number.isInteger(digit)) {
      return false;
    }
    const maxLength: number = this.length;
    if (this.stringValue.length === maxLength) {
      return true;
    }
    return this.wouldExceed(digit) || this.wouldFail(digit);
  }
  private wouldExceed(digit: number): boolean {
    if (!this.max) {
      return false;
    }
    let expectedValue: string = this.stringValue + digit;
    if (this.length) {
      const remainingDigits =
        this.length - this.stringValue.length - 1;
      expectedValue += new Array(remainingDigits)
        .fill('0')
        .join('');
    }
    return parseInt(expectedValue) > this.max;
  }
  private wouldFail(digit: number): boolean {
    if (!this.max || !this.length) {
      return false;
    }
    const expectedValue: string = this.stringValue + digit;
    const availableDigits: number =
      this.length - expectedValue.length;
    const highestPossibleValue: string =
      expectedValue +
      new Array(availableDigits).fill('9').join('');
    return parseInt(highestPossibleValue) < this.min;
  }

  private reset(): void {
    delete this.min;
    delete this.max;
    delete this.length;
  }
}
