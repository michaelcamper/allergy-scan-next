export interface KeyboardOptions {
  label: string;
  value?: number;
  unit?: string;
  range?: number[];
}
