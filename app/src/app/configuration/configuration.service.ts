import { Injectable } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { ConfigurationComponent } from './configuration.component';
import { DeviceOS, ITestGroup, User, ViewMode } from '../../../../models/user';
import { I18NService } from '../core/i18n.service';
import { AllergyProfile } from '../../../../models/allergy-profile';

/**
 * Spez
 * - Get Allergy Profile
 * - get Details
 * - Assign to testGroup
 * - Get Lang
 * - Get DeviceOS
 */

@Injectable()
export class ConfigurationService {
  get deviceOS(): DeviceOS {
    if (this.platform.is('ios')) {
      return 'ios';
    }
    if (this.platform.is('android')) {
      return 'android';
    }
    return null;
  }

  constructor(
    private modal: ModalController,
    private platform: Platform,
    private i18n: I18NService
  ) {}

  public async init(): Promise<
    | {
        config: User.IConfig;
        details: User.IDetails;
        deviceOS: DeviceOS;
      }
    | any
  > {
    const { allergies, details } = await this.openModal();

    const testGroup = ConfigurationService.testGroup();

    const config: User.IConfig = {
      allergies,
      lang: this.i18n.lang,
      testGroup,
    };
    return {
      config,
      details,
      deviceOS: this.deviceOS,
    };
  }

  private async openModal<T>(
    props?: Partial<ConfigurationComponent>
  ): Promise<ConfigurationData> {
    const modal = await this.modal.create({
      component: ConfigurationComponent,
      componentProps: props,
    });
    await modal.present();
    const data: ConfigurationData = (await modal.onDidDismiss()).data;
    return data || this.openModal({ ...props, dismissed: true });
  }

  private static testGroup(): ITestGroup {
    const randomBoolean = () => <boolean>(Math.random() >= 0.5);
    const tailored: boolean = randomBoolean();
    const view: ViewMode = randomBoolean() ? 'text' : 'icon';
    return { tailored, view };
  }
}

export interface ConfigurationData {
  allergies?: AllergyProfile;
  details: User.IDetails;
}
