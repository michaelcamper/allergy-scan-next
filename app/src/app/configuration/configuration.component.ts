import { Component, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../../models/user';
import { KeyboardOptions } from './digit-keyboard/digit-keyboard.model';
import { DigitKeyboardComponent } from './digit-keyboard/digit-keyboard.component';
import { map, startWith, take } from 'rxjs/operators';
import { ModalController } from '@ionic/angular';
import { ConfigurationData } from './configuration.service';
import { LegacyService } from '../core/legacy.service';
import { AllergyProfile } from '../../../../models/allergy-profile';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
})
export class ConfigurationComponent {
  dismissed: boolean;
  detailsOnly: boolean;

  allergies: AllergyProfile = {};
  details: FormGroup = ConfigurationComponent.generateForm();

  get introSlug(): string {
    if (this.dismissed) {
      return 'dismissed';
    }
    if (this.detailsOnly) {
      return 'detailsOnly';
    }
    return 'intro';
  }

  @ViewChild(DigitKeyboardComponent)
  keyboard: DigitKeyboardComponent;

  constructor(private modal: ModalController, legacy: LegacyService) {
    this.detailsOnly = legacy.detailsMissing;
    this.progress$.subscribe();
  }

  static generateForm(): FormGroup {
    const controls: { [key in keyof User.IDetails]: FormControl } = {
      gender: new FormControl(null, Validators.required),
      year: new FormControl(null, Validators.required),
      german: new FormControl(null, Validators.required),
      travelling: new FormControl(null),
      dietResearch: new FormControl(null),
    };
    return new FormGroup(controls);
  }

  public async showKeyboard({
    control,
    options,
  }: {
    control: keyof User.IDetails;
    options: KeyboardOptions;
  }) {
    this.keyboard.prompt(options);
    this.keyboard.set
      .pipe(take(1))
      .subscribe((value) => this.details.get(control).setValue(value));
    // this.details.get(control).setValue(value);
  }

  get progress$(): Observable<number> {
    return this.details.valueChanges.pipe(
      startWith(this.details.value),
      map(() => {
        const controls = Object.values(this.details.controls);
        const { total, valid } = controls.reduce(
          (state, control: AbstractControl) => {
            if (control.validator == null) {
              return state;
            }
            state.total++;
            if (control.valid) {
              state.valid++;
            }
            return state;
          },
          { total: 0, valid: 0 }
        );
        return valid / total;
      })
    );
  }

  public onSubmit() {
    const data: ConfigurationData = {
      allergies: this.allergies,
      details: this.details.value,
    };
    this.modal.dismiss(data);
  }
}
