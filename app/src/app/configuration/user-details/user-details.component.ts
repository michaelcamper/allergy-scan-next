import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { KeyboardOptions } from '../digit-keyboard/digit-keyboard.model';
import { User } from '../../../../../models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent {
  @Input()
  form: FormGroup;

  @Output()
  openKeyboard: EventEmitter<{
    control: keyof User.IDetails;
    options: KeyboardOptions;
  }> = new EventEmitter<{
    control: keyof User.IDetails;
    options: KeyboardOptions;
  }>();

  @Output()
  submit: EventEmitter<void> = new EventEmitter<void>();

  get genderControl(): FormControl {
    return this.form.get('gender') as FormControl;
  }
  get yearControl(): FormControl {
    return this.form.get('year') as FormControl;
  }

  get year(): string {
    return this.yearControl.valid
      ? this.yearControl.value
      : '• • • •';
  }

  setYear(): void {
    const options: KeyboardOptions = {
      label: `registration.body.year`,
      value: this.yearControl.value,
      range: [1900, new Date().getFullYear()],
    };
    this.openKeyboard.emit({
      control: 'year',
      options,
    });
  }
}
