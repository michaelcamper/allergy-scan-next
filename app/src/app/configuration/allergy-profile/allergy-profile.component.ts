import { Component, Input } from '@angular/core';
import { ALLERGENS } from '../../../../../config/allergens';
import { AllergyProfile } from '../../../../../models/allergy-profile';

@Component({
  selector: 'app-allergy-profile',
  templateUrl: './allergy-profile.component.html',
  styleUrls: ['./allergy-profile.component.scss'],
})
export class AllergyProfileComponent {
  public allergens: string[] = ALLERGENS;

  @Input()
  model: AllergyProfile = {};

  public toggleAllergen(key: string): void {
    this.model[key] = !this.model[key];
  }
}
