import { enumToArray } from '../utils';

export enum Allergen {
  allergenCellery = 'celery',
  allergenCelery = 'celery',
  allergenCrustacean = 'crustacean',
  allergenEggs = 'eggs',
  allergenFish = 'fish',
  allergenGluten = 'gluten',
  allergenLupin = 'lupin',
  allergenMilk = 'milk',
  allergenMolluscs = 'molluscs',
  allergenMustard = 'mustard',
  allergenPeanuts = 'peanuts',
  allergenSesameseeds = 'sesameseeds',
  allergenSoy = 'soy',
  allergenSulphites = 'sulphites',
  allergenTreenuts = 'treenuts',
}

export const ALLERGENS = enumToArray(Allergen);

export type AllergyProfile = { [K in Allergen]?: boolean };
