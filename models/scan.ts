import { Product } from '../functions/src/eatfit-api/product';

export namespace Scan {
  export interface Result {
    gtin: string;
    product?: Product;
  }

  export interface Document extends Result {
    timeStamp: number;
    date?: any; // legacy
  }

  export interface Entry extends Document {
    count: number;
  }
}
