import * as firebase from 'firebase';

export namespace Feedback {
  import DocumentReference = firebase.firestore.DocumentReference;

  export interface Data {
    [construct: string]: {
      [aspect: string]: number;
    };
  }
  export interface Document {
    user: DocumentReference;
    timeStamp: number;
    data: Data;
  }
}
