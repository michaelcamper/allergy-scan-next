import * as firestore from 'firebase/firestore';
import { AllergyProfile } from './allergy-profile';

export namespace User {
  import DocumentReference = firestore.DocumentReference;

  export interface IConfig {
    lang: string; // TODO guard with to AVAILABLE_LANGUAGES
    allergies: AllergyProfile;
    testGroup: ITestGroup;
  }

  export interface Document extends IConfig {
    timeStamp: number;
    deviceOS: DeviceOS;
    details: IDetails;
    data?: DocumentReference;
    feedback?: DocumentReference;
  }

  export interface IDetails {
    gender: Gender;
    year: number;
    german: LanguageSkills;
    travelling: boolean;
    dietResearch: boolean;
  }
}

/** Test Group **/
export interface ITestGroup {
  tailored: boolean;
  view: ViewMode;
}

export type ViewMode = 'text' | 'icon';
export type DeviceOS = 'ios' | 'android';

/** Details **/

type Gender = 'male' | 'female';
type LanguageSkills = 'none' | 'basics' | 'good' | 'native';
