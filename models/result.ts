import { Product } from '../functions/src/eatfit-api/product';
import { ViewMode } from './user';

export interface IResult extends Product {
  view: ViewMode;
  tailored: boolean;
}
