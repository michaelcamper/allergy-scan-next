import { User } from "./user";

export interface IAppConfig extends User.IConfig {
  docPath: string;
}
