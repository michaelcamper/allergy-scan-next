import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root',
})
export class FileLoaderService {
  constructor(private http: HttpClient, private papa: Papa) {}

  async load(
    name: 'allergen' | 'categories' | 'nutrition_facts' | 'product' | 'product_minor'
  ): Promise<CSVFile | ICSVFile> {
    const path = `assets/${name}.csv`;
    const text = await this.http
      .get(path, {
        responseType: 'text',
      })
      .toPromise();
    const { data } = this.papa.parse(text);
    const [cols, ...items] = data;
    return {
      cols,
      data: (items as string[][]).map((values) => {
        return (cols as string[]).reduce(
          (item, prop, index) => ({
            ...item,
            [prop]: values[index],
          }),
          {}
        );
      }),
    };
    // return new CSVFile(text);
  }
}

export interface ICSVFile {
  cols: string[];
  data: object[];
}
class CSVFile {
  cols: string[];
  items: string[];
  data: object[];
  constructor(text: string) {
    const [cols, ...items] = text.split('\n');
    this.cols = cols.split(',');
    this.items = items;

    const separator = '#';
    this.data = items
      // .slice(0, 100)
      .map((item) => {
        const data = item
          // .replace(/(,|#)\S/g, (string) => `${separator}${string[1]}`);
          .split(separator);
        return data.reduce((object, value, index) => {
          const prop = this.cols[index];
          return {
            ...object,
            [prop]: value,
          };
        }, {});
      });
  }
}
