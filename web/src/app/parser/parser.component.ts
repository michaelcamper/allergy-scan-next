import { Component, Input } from '@angular/core';
import { FileLoaderService } from '../file-loader.service';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { MatBottomSheet } from '@angular/material';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
  selector: 'app-parser',
  templateUrl: './parser.component.html',
  styleUrls: ['./parser.component.scss'],
})
export class ParserComponent {
  itemsLeft$: Observable<number>;
  categories$: Observable<ICategory[]>;
  majors$: Promise<IMajor[]>;

  constructor(
    private fileLoader: FileLoaderService,
    private api: ApiService,
    private db: AngularFirestore,
    private sheet: MatBottomSheet
  ) {
    // this.parseAmount();
  }

  async parseAmount() {
    const isSimpleGramm = (weight: string) => {
      return /^\d+([.,]\d+)?(\s| )?(gramm|gr.|gr|g)$/gi.test(weight);
    };
    const isMilliGramm = (weight: string) => {
      return /^\d+([.,]\d+)?(\s| )?(mg)$/gi.test(weight);
    };
    const isGrammWithTextTail = (weight: string) => {
      return /^\d+([.,]\d+)?(\s| )?(gramm|gr.|gr|g)\D+$/gi.test(weight);
    };
    const isGrammWithBracketTail = (weight: string) => {
      return /^\d+(\s| )?(gramm|gr.|gr|g)(\s| )?\(.*\)$/gi.test(weight);
    };
    const isGrammTotalStart = (weight: string) => {
      return /^\d+(\s| )?(gramm|gr.|gr|g)(\s| )?=.*$/gi.test(weight);
    };
    const isGrammTotalEnd = (weight: string) => {
      return (
        /.*=(\s| )?\d+(\s| )?(gramm|gr.|gr|g)$/gi.test(weight) &&
        !/\d(\s| )?ml/gi.test(weight)
      );
    };
    const startsWithGramm = (weight: string) => {
      return /^\d+(\s| )?(gramm|gr.|gr|g),/gi.test(weight);
    };
    const isKilo = (weight: string) => {
      return /^\d+([,.]\d+)?(\s| )?(kilo|kg)$/gi.test(weight);
    };

    const isKiloWithTextTail = (weight: string) => {
      return /^\d+([,.]\d+)?(\s| )?(kg)\D+$/gi.test(weight);
    };
    const isUnsolvedGrammMuliplikation = (weight: string) => {
      return (
        /^\d+(\s| )?(x|×|\*)(\s| )?\d+([.,]\d+)?(\s| )?(gramm|g)/gi.test(weight) &&
        !/ml/gi.test(weight) &&
        weight.match(/(x|×|\*)/gi) &&
        weight.match(/(x|×|\*)/gi).length === 1
      );
    };
    const isMilliliter = (weight: string) => {
      return /^\d+([,.]\d+)?(\s| )?(Mililiter|Milliliter|ml)$/gi.test(weight);
    };

    const isMilliWithBracketTail = (weight: string) => {
      return /^\d+(\s| )?(Mililiter|Milliliter|ml)(\s| )?\(.*\)$/gi.test(weight);
    };
    const isMilliWithTextTail = (weight: string) => {
      return /^\d+(\s| )?(Mililiter|Milliliter|ml)\D+$/gi.test(weight);
    };
    const isCenti = (weight: string) => {
      return /^\d+([.,]\d+)?(\s| )?(cl.|cl)$/gi.test(weight);
    };
    const isDeci = (weight: string) => {
      return /^\d+([.,]\d+)?(\s| )?dl$/gi.test(weight);
    };
    const isLiter = (weight: string) => {
      return /^(\d+|o)([.,](\s)?\d+)?(\s| )?(liter|litre|litro|lieter|ltr|l)/gi.test(
        weight
      );
    };

    const isUnsolvedMiliMuliplikation = (weight: string) => {
      return (
        /^\d+(\s| )?(x|×|\*)(\s| )?\d+([.,]\d+)?(\s| )?(Mililiter|Milliliter|ml)/gi.test(
          weight
        ) &&
        weight.match(/(x|×|\*)/gi) &&
        weight.match(/(x|×|\*)/gi).length === 1
      );
    };
    const isUnsolvedCentiMuliplikation = (weight: string) => {
      return (
        /^\d+(\s| )?(x|×|\*)(\s| )?\d+([.,]\d+)?(\s| )?(cl.|cl)/gi.test(weight) &&
        weight.match(/(x|×|\*)/gi) &&
        weight.match(/(x|×|\*)/gi).length === 1
      );
    };
    const isLiterWithTextTail = (weight: string) => {
      return /^\d+([,.]\d+)?(\s| )?(liter|litre|litro|lieter|ltr|l)\D+$/gi.test(weight);
    };
    const isUnsolvedLiterMuliplikation = (weight: string) => {
      return (
        /^\d+(\s| )?(x|×|\*)(\s| )?\d+([.,]\d+)?(\s| )?(liter|litre|litro|lieter|ltr|l)/gi.test(
          weight
        ) &&
        weight.match(/(x|×|\*)/gi) &&
        weight.match(/(x|×|\*)/gi).length === 1
      );
    };
    const startsWithMilli = (weight: string) => {
      return /^\d+(\s| )?(Milliliter|ml),/gi.test(weight);
    };
    const startsWithLiter = (weight: string) => {
      return /^\d+([,.]\d+)?(\s| )?(liter|litre|litro|lieter|ltr|l),/gi.test(weight);
    };

    const isDigitsOnly = (weight: string) => {
      return /^\d+([,.]\d+)?$/gi.test(weight);
    };

    const isPiecesOnly = (weight: string) => {
      return /^\d+\s(stück|st|stk|pcs|pc)$/gi.test(weight);
    };
    const piecesAndGramm = (weight: string) => {
      return /^\d+\s?(stück),\s\d+\s?g/gi.test(weight) && !/ml/gi.test(weight);
    };

    const isInvalid = (weight: string) => {
      return /^([\D\W]+|\D+)$/gi.test(weight);
    };

    // TODO Abtropfgewicht

    const { data, cols } = await this.fileLoader.load('product_minor');
    console.log(cols);
    const items = data
      // .slice(0, 5000)
      .reduce<Item[]>((all, item: Item) => {
        let weight = item.weight;
        if (!weight || isPiecesOnly(weight) || isInvalid(weight)) {
          item.weightInteger = null;
          item.weightUnit = null;
          return [...all, item];
        }
        weight = item.weight.replace('"', '');
        if (
          isSimpleGramm(weight) ||
          isGrammWithTextTail(weight) ||
          isGrammWithBracketTail(weight) ||
          isGrammTotalStart(weight) ||
          startsWithGramm(weight)
        ) {
          item.weightUnit = 'g';
          item.weightInteger = parseFloat(weight.replace(',', '.'));

          return [...all, item];
        }
        if (isMilliGramm(weight)) {
          item.weightUnit = 'g';
          item.weightInteger = parseFloat(weight.replace(',', '.')) / 1000;
          return [...all, item];
        }

        if (isUnsolvedGrammMuliplikation(weight)) {
          item.weightUnit = 'g';
          item.weightInteger = weight
            .replace(' ', '')
            .split('(x|×|*)')
            .reduce((product, part) => {
              return product * parseFloat(part.replace(',', '.'));
            }, 1);
          return [...all, item];
        }
        if (isGrammTotalEnd(weight)) {
          const total = weight.split('=').pop();
          item.weightUnit = 'g';
          item.weightInteger = parseInt(total);
          return [...all, item];
        }
        if (isKilo(weight) || isKiloWithTextTail(weight)) {
          item.weightUnit = 'g';
          item.weightInteger = parseFloat(weight.replace(',', '.')) * 1000;
          return [...all, item];
        }
        if (
          isMilliliter(weight) ||
          startsWithMilli(weight) ||
          isMilliWithTextTail(weight) ||
          isMilliWithBracketTail(weight)
        ) {
          item.weightUnit = 'ml';
          item.weightInteger = parseFloat(weight.replace(', ', '.').replace(',', '.'));
          return [...all, item];
        }
        if (isCenti(weight)) {
          item.weightUnit = 'ml';
          item.weightInteger = parseFloat(weight.replace(',', '.')) * 100;
          return [...all, item];
        }
        if (isDeci(weight)) {
          item.weightUnit = 'ml';
          item.weightInteger = parseFloat(weight.replace(',', '.')) * 10;
          return [...all, item];
        }
        if (isLiter(weight) || startsWithLiter(weight) || isLiterWithTextTail(weight)) {
          item.weightUnit = 'ml';
          item.weightInteger =
            parseFloat(
              weight
                .replace('O', '0')
                .replace(', ', '.')
                .replace(',', '.')
            ) * 1000;
          return [...all, item];
        }

        if (isUnsolvedMiliMuliplikation(weight)) {
          item.weightUnit = 'ml';
          item.weightInteger = weight
            .replace(' ', '')
            .split('(x|×|*)')
            .reduce((product, part) => {
              return product * parseFloat(part.replace(',', '.'));
            }, 1);
          return [...all, item];
        }

        if (isUnsolvedCentiMuliplikation(weight)) {
          item.weightUnit = 'ml';
          item.weightInteger =
            weight
              .replace(' ', '')
              .split('(x|×|*)')
              .reduce((product, part) => {
                return product * parseFloat(part.replace(',', '.'));
              }, 1) * 100;
          return [...all, item];
        }
        if (isUnsolvedLiterMuliplikation(weight)) {
          item.weightUnit = 'ml';
          item.weightInteger =
            weight
              .replace(' ', '')
              .split('(x|×|*)')
              .reduce((product, part) => {
                const factor = parseFloat(part.replace(',', '.'));
                return product * factor;
              }, 1) * 1000;
          return [...all, item];
        }
        if (isDigitsOnly(weight)) {
          item.weightUnit = null;
          item.weightInteger = parseFloat(weight.replace(',', '.'));
          return [...all, item];
        }

        if (piecesAndGramm(weight)) {
          const grams = parseInt(weight.replace(/\d+\s?(stück),\s/gi, ''));
          item.weightInteger = grams;
          item.weightUnit = 'g';
          return [...all, item];
        }

        return [
          ...all,
          {
            ...item,
            weightInteger: null,
            weightUnit: null,
          },
        ];
      }, []);
    // console.log(items);
    // console.log(items.map((item) => [item.weightUnit, item.weightInteger, item.weight]));

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: cols,
      nullToEmptyString: true,
    };

    new Angular5Csv(items, 'product_final', options);
  }

  async eliminateDUplictes() {
    const { data, cols } = await this.fileLoader.load('allergen');
    console.log(data);
    const items = data.reduce<any[]>((uniq, item, index, items) => {
      const prev = items[index - 1];
      if (!prev || JSON.stringify(prev) !== JSON.stringify(item)) {
        return [...uniq, item];
      }
      return uniq;
    }, []);
    console.log(items);
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: cols,
      nullToEmptyString: true,
    };
    new Angular5Csv(items, 'allergen_final', options);
  }

  async map() {
    const minors = await this.api.fetchMinors();
    const oldCategories: { name: string; id: number }[] = (await this.db
      .collection('categories')
      .ref.get()).docs.reduce((docs, doc) => {
      const data = doc.data();
      if (!data.id) {
        return docs;
      }
      return [...docs, data];
    }, []);

    const { cols, data } = await this.fileLoader.load('product');

    let missing = 0;

    const items = data
      // .slice(0, 1000)
      .map((item: { category: string; major: number; minor: number }) => {
        const { category } = item;
        if (!category) {
          return {
            ...item,
            major: null,
            minor: null,
          };
        }
        const oldCategory = oldCategories.find(
          ({ id, name }) => name.replace('"', '') === category.replace('"', '')
        );
        if (!oldCategory) {
          missing++;
          return {
            ...item,
            major: null,
            minor: null,
          };
        }
        const minor = oldCategory.id;
        item.minor = minor;
        const major = minors.find(({ id }) => id === minor).category_major;
        item.major = major;
        return item;
      });

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: [...cols, 'weightUnit', 'weightInteger'],
      nullToEmptyString: true,
    };

    new Angular5Csv(items, 'product_minor', options);
  }
}

export interface IMajor {
  id: number;
  name: string;
  minors: IMinor[];
}

export interface IMinor {
  id: number;
  name: string;
}

interface ICategory extends IMinor {
  ref: DocumentReference;
}

/*
 const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      noDownload: false,
      headers: cols,
      nullToEmptyString: true,
    };

    new Angular5Csv(data, 'allergens_unique', options);
 */

interface Item {
  weight: string;
  weightInteger: number;
  weightUnit: 'g' | 'ml';
}
