import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParserComponent } from './parser.component';
import { RouterModule } from '@angular/router';
import {
  MatBottomSheetModule,
  MatButtonModule,
  MatExpansionModule,
  MatIconModule,
  MatListModule,
  MatSelectModule,
  MatToolbarModule,
  MatAutocompleteModule,
  MatInputModule,
} from '@angular/material';
import { SelectorComponent } from './selector/selector.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PapaParseModule } from 'ngx-papaparse';

const MATERIAL_MODULES = [
  MatButtonModule,
  MatListModule,
  MatSelectModule,
  MatBottomSheetModule,
  MatToolbarModule,
  MatIconModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatInputModule,
];

@NgModule({
  declarations: [ParserComponent, SelectorComponent],
  entryComponents: [SelectorComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '**',
        component: ParserComponent,
      },
    ]),
    ReactiveFormsModule,
    ...MATERIAL_MODULES,
  ],
})
export class ParserModule {}
