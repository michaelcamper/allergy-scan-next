import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material';
import { IMajor, IMinor } from '../parser.component';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss'],
})
export class SelectorComponent implements OnInit, OnDestroy {
  hideAccordion: boolean;

  get name(): string {
    return this.data.name;
  }
  get majors(): IMajor[] {
    return this.data.majors;
  }

  get minors(): IMinor[] {
    return this.majors
      .reduce((minors, major) => [...minors, ...major.minors], [])
      .sort((a, b) => {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });
  }
  filteredMinors: IMinor[] = [];

  searchListener: Subscription;

  minorCtrl: FormControl = new FormControl();

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private data: { majors: IMajor[]; name: string },
    private sheet: MatBottomSheetRef<SelectorComponent>
  ) {}

  ngOnInit() {
    this.searchListener = this.minorCtrl.valueChanges.subscribe((value) => {
      const regex = new RegExp(value, 'gi');
      this.filteredMinors = this.minors.filter(({ name }) => name.match(regex));
    });
  }

  onSelect(id: number) {
    this.sheet.dismiss(id);
  }

  ngOnDestroy(): void {
    this.searchListener.unsubscribe();
  }
}
