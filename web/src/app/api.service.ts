import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  fetchMajors(): Promise<ICategory[]> {
    return this.http
      .get('https://eatfit-service.foodcoa.ch/category/major/', {
        headers: new HttpHeaders({
          Authorization: `Basic ${btoa(
            `eatfit_student_michael:cbbw9$4TS4qxXaU#1XDuHYW5`
          )}`,
        }),
      })
      .toPromise() as Promise<ICategory[]>;
  }
  fetchMinors(): Promise<ICategory[]> {
    return this.http
      .get('https://eatfit-service.foodcoa.ch/category/minor/', {
        headers: new HttpHeaders({
          Authorization: `Basic ${btoa(
            `eatfit_student_michael:cbbw9$4TS4qxXaU#1XDuHYW5`
          )}`,
        }),
      })
      .toPromise() as Promise<ICategory[]>;
  }
}

export interface ICategory {
  id: number;
  name_de: string;
  category_major?: number;
}
